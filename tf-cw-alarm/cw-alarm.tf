terraform {
  required_providers {
    aws = {
        source = "hashicorp/aws"
        version = "~> 4.16"
    }
  }

required_version = ">= 1.2.0"

}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_security_group" "my-sg" {
  name = "my-sg"
  description = "Allow SSH traffic"
  vpc_id = aws_default_vpc.default.id

    ingress {
        description = "SSH ingress allow"
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        description = "Egress allow all"
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

}

resource "aws_instance" "my-ec2" {
  ami = "ami-0ec7f9846da6b0f61"
  instance_type = "t2.micro"
  monitoring = true  # 1-min frames
  vpc_security_group_ids = [aws_security_group.my-sg.id]
}

resource "aws_sns_topic" "cpu-alarm-sns" {
  name = "cpu-alarm-sns"
}

resource "aws_sns_topic_subscription" "admin-sns-cpu-alarm" {
  topic_arn = aws_sns_topic.cpu-alarm-sns.arn
  protocol = "email"
  endpoint = "piotr.swiecik@gmail.com"
}

resource "aws_cloudwatch_metric_alarm" "cpu-alarm" {
  alarm_name = "MyEC2CPUUtilizationAlarm"
  alarm_description = "Triggers SNS notification when CPU usage exceeds 40%"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  metric_name = "CPUUtilization"
  namespace = "AWS/EC2"
  evaluation_periods = 4
  datapoints_to_alarm = 2
  period = "30"
  threshold = "40"
  statistic = "Average"
  dimensions = {
    InstanceId = aws_instance.my-ec2.id
  }
  alarm_actions = [aws_sns_topic.cpu-alarm-sns.arn]
}

