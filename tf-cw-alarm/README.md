# Terraform: CloudWatch Alarms

- Create Ubuntu EC2 instance.
- Create SSH access security group.
- Add CW Alarm to instance - CPU usage > 40%.
- Create SNS topic for alarm notifications.
